#include <iostream>
#include <variant>

using std::cout;
using std::endl;

namespace state
{
    struct A {};
    struct B {};
    struct C {};

    using any = std::variant<A,B,C>;
}

void transition(state::any & machine)
{
    if(std::holds_alternative<state::A>(machine))
    {
        cout << "state A" << endl;
        cout << "transition to B" << endl;
        machine = state::B{};
    }
    else if(std::holds_alternative<state::B>(machine))
    {
        cout << "state B" << endl;
        cout << "transition to C" << endl;
        machine = state::C{};
    }
    else if(std::holds_alternative<state::C>(machine))
    {
        cout << "state C" << endl;
        cout << "transition to A" << endl;
        machine = state::A{};
    }
}

int main()
{
    state::any machine = state::A{};

    for(int i=0; i<3; i++)
    {
        transition(machine);
    }
}
