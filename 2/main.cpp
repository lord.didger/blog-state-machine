#include <iostream>
#include <variant>

using std::cout;
using std::endl;

namespace state
{
    struct A {};
    struct B {};
    struct C {};

    using any = std::variant<A,B,C>;
}

struct Visitor
{
    state::any operator()(state::A &)
    {
        cout << "state A" << endl;
        cout << "transition to B" << endl;
        return state::B{};
    }
    state::any operator()(state::B &)
    {
        cout << "state B" << endl;
        cout << "transition to C" << endl;
        return state::C{};
    }
    state::any operator()(state::C &)
    {
        cout << "state C" << endl;
        cout << "transition to A" << endl;
        return state::A{};
    }
};

state::any transition(state::any & machine)
{
    return std::visit(Visitor{}, machine);
}

int main()
{
    state::any machine = state::A{};

    for(int i=0; i<3; i++)
    {
        machine = transition(machine);
    }
}
