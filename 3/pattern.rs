
enum Any
{
    A(i32),
    B(Vec<i32>),
    C(Vec<f32>)
}

fn process(any: Any)
{
    match any
    {
        Any::A(i) if i < 5 => println!("A < 5"),
        Any::A(_) => println!("A"),
        Any::B(_) => println!("B"),
        Any::C(_) => println!("C")
    }
}

fn main()
{
    process(Any::A(1));
    process(Any::A(6));
    process(Any::B(Vec::<i32>::new()));
    process(Any::C(Vec::<f32>::new()));
}
