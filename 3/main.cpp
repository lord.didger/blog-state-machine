#include <iostream>
#include <variant>

using std::cout;
using std::endl;

namespace state
{
    struct A {};
    struct B {};
    struct C {};

    using any = std::variant<A,B,C>;
}

template<typename ... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template<typename ... Ts>
overloaded(Ts... args) -> overloaded<Ts...>;

state::any transition(state::any & machine)
{
    return std::visit(
        overloaded
        {
            [](state::A &) -> state::any
            {
                cout << "state A" << endl;
                cout << "transition to B" << endl;
                return state::B{};
            },
            [](state::B &) -> state::any
            {
                cout << "state B" << endl;
                cout << "transition to C" << endl;
                return state::C{};
            },
            [](state::C &) -> state::any
            {
                cout << "state C" << endl;
                cout << "transition to A" << endl;
                return state::A{};
            }
        },
        machine
    );
}

int main()
{
    state::any machine = state::A{};

    for(int i=0; i<3; i++)
    {
        machine = transition(machine);
    }
}
