#include <iostream>
#include <vector>
using std::cout;
using std::endl;

struct Visitor
{
    void operator()(int)
    {
        cout << __PRETTY_FUNCTION__ << endl;
    }

    template<typename T>
    void operator()(std::vector<T>&)
    {
        cout << __PRETTY_FUNCTION__ << endl;
    }
};

int main()
{
    {
        Visitor()(1);
    }
    {
        std::vector<int> c;
        Visitor()(c);
    }
    {
        std::vector<double> c;
        Visitor()(c);
    }
}
